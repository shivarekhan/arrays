function map(elements, callback) {
    // Do NOT use .map, to complete this function.
    // How map works: Map calls a provided callback function once for each element in an array, in order, and functionructs a new array from the res .
    // Produces a new array of values by mapping each value in list through a transformation function (iteratee).
    // Return the new array.
    const arrLength = elements.length;
    if(!Array.isArray(elements) || typeof callback !=="function" || arrLength===0 ){
        return[];
    }
    let arr = [];
    for (let index = 0; index < arrLength; index++) {
        arr.push(callback(elements[index],index,elements));
    }
    return arr;
}
module.exports = map;
