const find = require('../find.cjs');

let result1 = find([1, 2, 3, 4, 5, 5], function (num) { return num % 2 == 0; })
if (result1 == undefined) {
    console.log("no result found ");
}
else if (result1 === []) {
    console.log("Input array was empty");
}
else {
    console.log(result1);
}

let result2 = find([1, 2, 3, 4, 5, 5], function (num) { return num / 2 == 0; })
if (result2 == undefined) {
    console.log("no result found ");
}
else if (result1 === []) {
    console.log("Input array was empty");
}
else {
    console.log(result2);
}