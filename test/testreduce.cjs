const reduce = require('../reduce.cjs');
let result1 = reduce([1, 2, 3, 4, 5, 5], (startingValue, current) => startingValue + current, 22);
if (result1 == false) {
    console.log("the input is not array");
}
else {
    console.log(result1);
}

let result2 = reduce([1, 2, 3, 90, 98, 4, 5, 5], (startingValue, current) => startingValue + current, 22);
if (result2 == false) {
    console.log("the input is not array");
}
else {
    console.log(result2);
}

let result3 = reduce([1, 2, 3, 4, 5, 5], (startingValue, current) => startingValue + current);
if (result3 == false) {
    console.log("the input is not array");
}
else {
    console.log(result3);
}