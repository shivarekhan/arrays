const filter = require('../filter.cjs');
let result1 = filter([1, 2, 3, 4, 5, 5], function (num) { return num % 2 == 0; })
if (result1 == undefined) {
    console.log("no result found ");
}
else if (result1 == false) {
    console.log("No matching result found");
}
else {
    console.log(result1);
}


let result2 = filter([], function (num) { return num % 2 == 0; })
if (result2 == undefined) {
    console.log("no result found ");
}
else if (result2 == false) {
    console.log("No matching result found");
}
else {
    console.log(result2);
}

let result3 = filter([1, 2, 3, 6, 7, 8, 11, 14, 67, 14, 128, 4, 5, 5], function (num) { return num / 2 == 0; })
if (result3 == undefined) {
    console.log("no result found ");
}
else if (result3 == false) {
    console.log("No matching result found");
}
else {
    console.log(result3);
}