const each = require('../each.cjs');
let result1 = each([1,2,3,4,5],(x,i)=>x[i]*2);
if(result1==false){
    console.log("input array is empty");
}
else{
    console.log(result1);
}

let result2 = each([],(x,i)=>x[i]**2);
if(result2==false){
    console.log("input array is empty");
}
else{
    console.log(result2);
}

let result3 = each([2,3,4,5,6,7],(x,i)=>x[i]**2);
if(result3==false){
    console.log("input array is empty");
}
else{
    console.log(result3);
}
