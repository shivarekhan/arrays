function find(elements, callback) {
    // Do NOT use .includes, to complete this function.
    // Look through each value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
    const elementsLength = elements.length;
    if (elementsLength === 0 || typeof callback !== 'function' || !Array.isArray(elements)) {
        return [];
    }
    for (let index = 0; index < elementsLength; index++) {
        let buff = callback(elements[index], index, elements);
        if (buff == true) {
            return elements[index];
        }
    }
    return undefined;
}
module.exports = find;