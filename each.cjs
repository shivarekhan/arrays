function each(elements, callback) {
    // Do NOT use forEach to complete this function.
    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // This only needs to work with arrays.
    // You should also pass the index into `cb` as the second argument
    // based off http://underscorejs.org/#each
    if (elements.length == 0) {
        return false;
    }
    let auxArray = [];
    const lengthOfArray = elements.length;
    for (let index = 0; index < lengthOfArray; index++) {
        auxArray[index] = callback(elements,index);
    }
    return auxArray;


}
module.exports = each;