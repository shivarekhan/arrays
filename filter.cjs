function filter(elements, callback) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
    const arrLength = elements.length;
    if (arrLength === 0 || !Array.isArray(elements) || typeof callback !== 'function') {
        return [];
    }
    let resArr = [];
    for (let index = 0; index < arrLength; index++) {
        let buffArr = callback(elements[index],index,elements);
        if (buffArr == true) {
            resArr.push(elements[index]);
        }
    }
    return resArr;
}
module.exports = filter;