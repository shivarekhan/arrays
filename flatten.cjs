function flatten(elements, depth = undefined) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    const elementsLength = elements.length;
    if (elementsLength === 0 || !Array.isArray(elements)) {
        return [];
    }
    if (depth === undefined) {
        depth = 1;
    }
    let resarr = [];
    for (let index = 0; index < elementsLength; index++){
        if(Array.isArray(elements[index]) && depth!=0){
            resarr = resarr.concat(flatten(elements[index],depth-1));
        }else if(elements[index]===undefined){
            continue;
        }else{
            resarr.push(elements[index]);
        }
    }
    return resarr;
    
}
module.exports = flatten;